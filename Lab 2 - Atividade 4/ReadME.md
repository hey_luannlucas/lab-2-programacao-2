# Atividade #4 - Relações de Associação, Agregação e Composição

Este projeto em Java é um exemplo de Programação Orientada a Objetos (OOP) que demonstra a criação e interação com animais de estimação de diferentes tipos. Ele ilustra o uso de classes abstratas, interfaces e herança para modelar o comportamento dos animais e permite que um jogador adquira e interaja com os mascotes.

## Conceitos OOP Utilizados:

O projeto demonstra os seguintes conceitos de OOP:

- **Classes Abstratas**: A classe abstrata `Mascote` fornece métodos comuns a todos os animais de estimação, como `comer()`, `dormir()` e `treinar()`. Ela também declara um método abstrato `mover()`, que é implementado pelas subclasses para fornecer o comportamento de movimento específico de cada animal.

- **Interfaces**: As interfaces `PodeAndar`, `PodeVoar` e `PodeNadar` definem os comportamentos de andar, voar e nadar, respectivamente. As classes de animais de estimação implementam essas interfaces para indicar os comportamentos que cada animal pode realizar.

- **Herança**: As classes de animais de estimação, como `Gato`, `Pato`, `Lagarto`, `Falcao` e `Morcego`, estendem a classe abstrata `Mascote` e implementam as interfaces apropriadas para herdar e fornecer comportamentos específicos.

- **Polimorfismo**: O polimorfismo é demonstrado através da lista de mascotes mantida pelo jogador. O jogador pode adquirir mascotes de diferentes tipos e interagir com eles de forma genérica, utilizando os métodos fornecidos pela classe `Mascote`.

## Diagrama de classes:
![diagrama de classes](Assets/Diagrama%20de%20Classes%20-%20Atv4.png)

## Funcionalidades e Uso:

O projeto permite que você crie jogadores, adquira mascotes e interaja com eles. Aqui estão as funcionalidades principais:

- **Criação de Jogadores**: Você pode criar um jogador utilizando a classe `Jogador` e fornecendo um nome para o jogador.

- **Adquirir Mascotes**: Utilize o método `adquirirMascote()` da classe `Jogador` para adquirir um mascote. Passe uma instância de um dos animais de estimação disponíveis, como `Gato`, `Pato`, `Lagarto`, `Falcao` ou `Morcego`.

- **Exibir Mascotes**: Use o método `exibirMascotes()` da classe `Jogador` para exibir os mascotes adquiridos pelo jogador, juntamente com os comportamentos associados.

## Exemplo de Uso:

```java
public class Main {
    public static void main(String[] args) {
        // Criação de jogadores
        Jogador jogador1 = new Jogador("Jogador 1");
        Jogador jogador2 = new Jogador("Jogador 2");

        // Criação de mascotes
        Mascote m1 = new Gato();
        Mascote m2 = new Pato();
        Mascote m3 = new Lagarto();
        Mascote m4 = new Falcao();
        Mascote m5 = new Morcego();

        // Adquirir mascotes
        jogador1.adquirirMascote(m1);
        jogador1.adquirirMascote(m2);
        jogador2.adquirirMascote(m3);
        jogador2.adquirirMascote(m4);
        jogador2.adquirirMascote(m5);

        // Exibir mascotes
        jogador1.exibirMascotes();
        jogador2.exibirMascotes();
    }
}
```


![execução](Assets/mascote.gif)
