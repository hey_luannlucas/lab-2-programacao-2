class Falcao extends Mascote implements PodeAndar, PodeVoar {
    @Override
    public String mover() {
        return voar() + ", " + andar();
    }
}