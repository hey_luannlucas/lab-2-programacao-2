public class Main {
    public static void main(String[] args) {
        Jogador jogador1 = new Jogador("Jogador 1");
        Jogador jogador2 = new Jogador("Jogador 2");

        Mascote m1 = new Gato();
        Mascote m2 = new Pato();
        Mascote m3 = new Lagarto();
        Mascote m4 = new Falcao();
        Mascote m5 = new Morcego();

        jogador1.adquirirMascote(m1);
        jogador1.adquirirMascote(m2);
        jogador2.adquirirMascote(m3);
        jogador2.adquirirMascote(m4);
        jogador2.adquirirMascote(m5);

        jogador1.exibirMascotes();
        jogador2.exibirMascotes();
    }
}