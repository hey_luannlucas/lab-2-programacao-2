abstract class Mascote {
    public void comer() {
        System.out.println("O mascote está comendo.");
    }

    public void dormir() {
        System.out.println("O mascote está dormindo.");
    }

    public void treinar() {
        System.out.println("O mascote está treinando.");
    }

    public abstract String mover();
}
interface PodeAndar {
    default String andar() {
        return "pode andar";
    }
}

interface PodeVoar {
    default String voar() {
        return "pode voar";
    }
}

interface PodeNadar {
    default String nadar() {
        return "pode nadar";
    }
}