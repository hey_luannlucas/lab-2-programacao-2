import java.util.List;
import java.util.ArrayList;
class Jogador {
    private List<Mascote> mascotes;
    private String nome;

    public Jogador(String nome) {
        this.nome = nome;
        mascotes = new ArrayList<>();
    }

    public void adquirirMascote(Mascote mascote) {
        mascotes.add(mascote);
    }

    public void exibirMascotes() {
        System.out.println("O jogador " + nome + " tem animais de estimação:");
        for (Mascote mascote : mascotes) {
            System.out.println(mascote.getClass().getSimpleName() + ": " + mascote.mover());
        }
        System.out.println();

    }
}