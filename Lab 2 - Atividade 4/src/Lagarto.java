class Lagarto extends Mascote implements PodeAndar, PodeVoar {
    @Override
    public String mover() {
        return andar() + ", " + voar();
    }
}