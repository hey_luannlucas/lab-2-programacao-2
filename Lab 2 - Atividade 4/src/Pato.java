class Pato extends Mascote implements PodeAndar, PodeVoar, PodeNadar {
    @Override
    public String mover() {
        return andar() + ", " + voar() + ", " + nadar();
    }
}