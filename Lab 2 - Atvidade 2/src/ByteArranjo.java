import java.util.Arrays;

class ByteArranjo extends Arranjo {
    public ByteArranjo(Byte[] arr) {
        super(arr);
    }

    @Override
    public void classificar() {
        Arrays.sort(arr, (a, b) -> ((Byte) b).compareTo((Byte) a));
    }

    @Override
    public String getNome() {
        return "ByteArranjo";
    }
}