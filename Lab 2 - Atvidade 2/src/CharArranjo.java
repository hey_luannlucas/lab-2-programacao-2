import java.util.Arrays;

class CharArranjo extends Arranjo {
    public CharArranjo(Character[] arr) {
        super(arr);
    }

    @Override
    public void classificar() {
        Arrays.sort(arr, (a, b) -> ((Character) b).compareTo((Character) a));
    }

    @Override
    public String getNome() {
        return "CharArranjo";
    }
}