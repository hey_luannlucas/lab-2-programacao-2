import java.util.Arrays;
class ShortArranjo extends Arranjo {
    public ShortArranjo(Short[] arr) {
        super(arr);
    }

    @Override
    public void classificar() {
        Arrays.sort(arr, (a, b) -> ((Short) b).compareTo((Short) a));
    }

    @Override
    public String getNome() {
        return "ShortArranjo";
    }
}