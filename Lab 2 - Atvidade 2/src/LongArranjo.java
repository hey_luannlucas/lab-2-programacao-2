import java.util.Arrays;
class LongArranjo extends Arranjo {
    public LongArranjo(Long[] arr) {
        super(arr);
    }

    @Override
    public void classificar() {
        Arrays.sort(arr, (a, b) -> ((Long) b).compareTo((Long) a));
    }
    @Override
    public String getNome() {
        return "LongArranjo";
    }
}