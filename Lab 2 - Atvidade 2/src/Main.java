public class Main {
    public static void main(String[] args) {
        // Integer[]
        Integer[] arr1 = {2, 1, 6, 8, 3};
        IntArranjo intArranjo1 = new IntArranjo(arr1);
        System.out.println(intArranjo1.getNome());
        intArranjo1.listar();
        intArranjo1.classificar();
        intArranjo1.listar();
        System.out.println();

        // Float[]
        Float[] arr2 = {0.2f, 0.5f, 1.2f, 2.5f};
        FloatArranjo floatArranjo = new FloatArranjo(arr2);
        System.out.println(floatArranjo.getNome());
        floatArranjo.listar();
        floatArranjo.classificar();
        floatArranjo.listar();
        System.out.println();

        // Double[]
        Double[] arr3 = {0.2, 0.5, 1.2, 2.5};
        DoubleArranjo doubleArranjo = new DoubleArranjo(arr3);
        System.out.println(doubleArranjo.getNome());
        doubleArranjo.listar();
        doubleArranjo.classificar();
        doubleArranjo.listar();
        System.out.println();

        // Byte[]
        Byte[] arr4 = {2, 5, 1, 3};
        ByteArranjo byteArranjo = new ByteArranjo(arr4);
        System.out.println(byteArranjo.getNome());
        byteArranjo.listar();
        byteArranjo.classificar();
        byteArranjo.listar();
        System.out.println();

        // Short[]
        Short[] arr5 = {2, 5, 1, 3};
        ShortArranjo shortArranjo = new ShortArranjo(arr5);
        System.out.println(shortArranjo.getNome());
        shortArranjo.listar();
        shortArranjo.classificar();
        shortArranjo.listar();
        System.out.println();

        // Long[]
        Long[] arr6 = {2L, 5L, 1L, 3L};
        LongArranjo longArranjo = new LongArranjo(arr6);
        System.out.println(longArranjo.getNome());
        longArranjo.listar();
        longArranjo.classificar();
        longArranjo.listar();
        System.out.println();

        // Character[]
        Character[] arr7 = {'n', 'v', 'd', 'a'};
        CharArranjo charArranjo = new CharArranjo(arr7);
        System.out.println(charArranjo.getNome());
        charArranjo.listar();
        charArranjo.classificar();
        charArranjo.listar();
        System.out.println();
    }
}
