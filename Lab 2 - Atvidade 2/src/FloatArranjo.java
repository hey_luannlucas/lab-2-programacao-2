import java.util.Arrays;
class FloatArranjo extends Arranjo {
    public FloatArranjo(Float[] arr) {
        super(arr);
    }

    @Override
    public void classificar() {
        Arrays.sort(arr, (a, b) -> ((Float) b).compareTo((Float) a));
    }

    @Override
    public String getNome() {
        return "FloatArranjo";
    }
}