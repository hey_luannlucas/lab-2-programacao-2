import java.util.Arrays;
class IntArranjo extends Arranjo {
    public IntArranjo(Integer[] arr) {
        super(arr);
    }

    @Override
    public void classificar() {
        Arrays.sort(arr, (a, b) -> ((Integer) b).compareTo((Integer) a));
    }
    @Override
    public String getNome() {
        return "IntArranjo";
    }
}