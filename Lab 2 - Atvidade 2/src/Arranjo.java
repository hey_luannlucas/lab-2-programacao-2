import java.util.Arrays;
abstract class Arranjo {

    protected Comparable[] arr;

    public Arranjo(Comparable[] arr) {
        this.arr = arr;
    }

    public abstract void classificar();

    public void listar() {
        System.out.println(Arrays.toString(arr));
    }
    public void remover(int indice) {
        if (indice >= 0 && indice < arr.length) {
            Comparable[] novoArr = new Comparable[arr.length - 1];
            int contador = 0;
            for (int i = 0; i < arr.length; i++) {
                if (i != indice) {
                    novoArr[contador] = arr[i];
                    contador++;
                }
            }
            arr = novoArr;
        } else {
            System.out.println("Índice inválido!");
        }
    }

    public abstract String getNome();
}
