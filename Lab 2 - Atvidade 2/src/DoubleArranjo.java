import java.util.Arrays;

class DoubleArranjo extends Arranjo {
    public DoubleArranjo(Double[] arr) {
        super(arr);
    }

    @Override
    public void classificar() {
        Arrays.sort(arr, (a, b) -> ((Double) b).compareTo((Double) a));
    }

    @Override
    public String getNome() {
        return "DoubleArranjo";
    }
}