## Atividade #2 - Classificação Polimórfica

A solução é uma implementação em Java que permite criar e manipular arranjos numéricos de diferentes tipos (inteiros, ponto flutuante, duplo, byte, short, long e caractere). 
<br>Aqui ultilizo conceitos de herança, polimorfismo e sobrescrita de métodos.

### Classes Principais:

- `Arranjo`: Classe abstrata que serve como base para as subclasses que gerenciam os diferentes tipos de arranjos numéricos. 
<br>Possui um construtor para inicializar o arranjo e define os métodos abstratos `classificar()` e `getNome()`. 
<br>Além disso, possui os métodos `listar()` para imprimir os elementos do arranjo e `remover(indice)` para remover um elemento específico.
<br></br>

- Subclasses: Cada tipo numérico possui uma subclasse correspondente 
<br>por exemplo: `IntArranjo`, `FloatArranjo`, `DoubleArranjo`, `ByteArranjo`, `ShortArranjo`, `LongArranjo` e `CharArranjo`. 
<br>Essas subclasses herdam da classe `Arranjo` e implementam os métodos `classificar()` e `getNome()` de acordo com as regras de comparação específicas de cada tipo.

### Funcionalidades:

- Criação: O código permite criar instâncias de arranjos numéricos para cada tipo de dado suportado.
  <br></br>
- Classificação: O método `classificar()` é responsável por ordenar os elementos do arranjo em ordem decrescente, utilizando os operadores de comparação nativos de cada tipo numérico.
  <br></br>
- Listagem: O método `listar()` exibe os elementos do arranjo uma vez que tenham sido classificados.
  <br></br>
- Remoção de Itens: O método `remover(indice)` permite remover um elemento específico do arranjo com base em um índice fornecido.
  <br></br>

### Diagrama de classes:

![diagrama de classe](Assets/diagrama%20de%20classe%20atv2.png)

### Utilização do Código:

Para usar o código, pode-se criar instâncias das subclasses correspondentes a cada tipo de arranjo desejado. 
<br>Em seguida, é possível chamar os métodos `classificar()`, `getNome()`, `listar()` e `remover()` conforme você queira.

Exemplo de uso:
<div style="padding-right: 1000px;">
<pre>
<code>input:

Integer[] arr = {2, 1, 6, 8, 3};
IntArranjo intArranjo = new IntArranjo(arr);
intArranjo.classificar();
System.out.println(intArranjo.getNome());
intArranjo.listar();
intArranjo.remover(2);
</code>
</pre>
</div>

<div style="padding-right: 1000px;">
<pre>
<code>output:

IntArranjo
[8, 6, 3, 2, 1]
[8, 6, 2, 1]
</code>
</pre>
</div>

### Teste:
![minha execucao](Assets/executar-atv2.gif)