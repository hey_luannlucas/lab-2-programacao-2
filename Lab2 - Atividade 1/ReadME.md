# Tratamento de Exceções - Atividade 1

O programa implementa uma chamada "MySet" que permite adicionar números inteiros a um conjunto e lança exceções personalizadas em certas condições.
O método `add(int value)` na classe `MySet` é responsável por adicionar um novo valor ao conjunto. 

### Exceções lançadas pelo método:

1. `Exception("Número negativo inserido. Encerrando a execução.")`: Essa exceção é lançada se o valor passado para o método `add` for menor que zero. 
Isso evita que números negativos sejam adicionados ao conjunto.<br></br>

2. `Exception("Valor muito grande inserido. Encerrando a execução.")`: Essa exceção é lançada se o valor passado para o método `add` for maior que 1.000.000. 
Isso serve para restringir o tamanho dos valores adicionados ao conjunto.
<br></br>

3. `Exception("Este valor já existe na lista. Encerrando a execução.")`: Essa exceção é lançada se o valor passado para o método `add` já estiver presente no conjunto. 
Isso impede a adição de valores duplicados ao conjunto.
<br></br>

4. `Exception("A lista atingiu o limite máximo de 100 elementos. Encerrando a execução.")`: Essa exceção é lançada se o tamanho do conjunto atingir o limite máximo de 100 elementos. 
Isso garante que o conjunto não cresça além desse limite.
****
Após verificar todas as condições, se nenhuma exceção for lançada, o valor é adicionado ao conjunto e o tamanho do conjunto é incrementado.
****


## Descrição:

A classe `MySet` representa um conjunto de inteiros não classificados. 
Ela possui uma matriz interna para armazenar os elementos do conjunto.

A classe (`Main`) permite que o usuário insira números inteiros positivos a partir do console. Ele para de receber entradas quando um número negativo é inserido. O programa garante que os elementos inseridos sejam adicionados ao conjunto de forma segura, tratando exceções personalizadas se um valor for repetido, se for nulo ou muito grande, ou se o conjunto já estiver cheio.

## Diagrama de Classes:

<div style="display: flex; align-items: center;">
    <div style="flex: 1; padding-right: 100px; padding-top: 20px">
        <pre>
 class MySet {
    - int[] array
    - int size
    + MySet()
    + add(int value)
    - contains(int value): boolean
}

class Main {
+ main(String[] args)
}

MySet --&gt; Main
        </pre>
    </div>
![Diagrama de Classe](Assets/Myset-Diagrama%20de%20Classes.png)
</div>
</div>

## Execução
![executar](Assets/Execucao.gif)

### Para Número negativo: 
![Negativo](Assets/Número%20negativo%20inserido.%20Encerrando%20a%20execução..gif)

### Valor muito grande:
![Valor grande](Assets/Valor-muito-grande-inserido.-Encerrando-a-execução..gif)

### Se valor já existe:
![Valor existente](Assets/Este-valor-já-existe-na-lista.-Encerrando-a-execução..gif)

### Se elementos mais que 100:
Usei a função:


       try {
           for (int i = 1; i <= 101; i++) {
              set.add(i);
                System.out.println(i);
           }
       } catch (Exception e) {
            System.out.println();
           System.out.println(e.getMessage());
       }
    }

<small style="font-size: 12px;">*** Está comentado na classe Main</small>

![elemento max](Assets/A%20lista%20atingiu%20o%20limite%20máximo%20de%20100%20elementos.%20Encerrando%20a%20execução.gif)









