import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        MySet set = new MySet();
        Scanner scanner = new Scanner(System.in);

        int value;
        do {
            try {
                value = scanner.nextInt();
                set.add(value);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                break;
            }
        } while (true);
    }
}

// Comente o de cima e descomente esse para testar o número máximo de elementos

// public class Main {
//    public static void main(String[] args) {
//        MySet set = new MySet();
//
//        try {
//            for (int i = 1; i <= 101; i++) {
//                set.add(i);
//                System.out.println(i);
//            }
//        } catch (Exception e) {
//            System.out.println();
//            System.out.println(e.getMessage());
//        }
//    }
//}