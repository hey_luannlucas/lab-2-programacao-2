
class MySet {
    private int[] array;
    private int size;

    public MySet() {
        array = new int[100];
        size = 0;
    }

    public void add(int value) throws Exception {
        if (value < 0) {
            throw new Exception("Número negativo inserido. Encerrando a execução.");
        }

        if (value > 1000000) {
            throw new Exception("Valor muito grande inserido. Encerrando a execução.");
        }

        if (contains(value)) {
            throw new Exception("Este valor já existe na lista. Encerrando a execução.");
        }

        if (size >= 100) {
            throw new Exception("A lista atingiu o limite máximo de 100 elementos. Encerrando a execução.");
        }

        array[size] = value;
        size++;
    }

    private boolean contains(int value) {
        for (int i = 0; i < size; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }
}


