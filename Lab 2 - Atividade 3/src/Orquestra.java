import java.util.ArrayList;
import java.util.List;
class Orquestra {
    private final List<Instrumento> instrumentos;

    public Orquestra() {
        instrumentos = new ArrayList<>();
    }

    public void add(Instrumento instrumento) {
        instrumentos.add(instrumento);
    }

    public void play() {
        System.out.println("Orquestra:");
        for (Instrumento instrumento : instrumentos) {
            instrumento.play();
        }
    }

    public void play(int index) {
        if (index >= 0 && index < instrumentos.size()) {
            System.out.println(instrumentos.get(index).toString() + ":");
            instrumentos.get(index).play();
        } else {
            System.out.println("Índice inválido. O instrumento não existe na orquestra.");
        }
    }
}