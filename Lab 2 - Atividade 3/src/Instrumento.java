abstract class Instrumento {
    public abstract void play();

    @Override
    public abstract String toString();
}