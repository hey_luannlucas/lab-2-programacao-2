class Trompete extends Instrumento {
    private final String nota;

    public Trompete(String nota) {
        this.nota = nota;
    }

    @Override
    public void play() {
        System.out.println("Toca na nota " + nota);
    }

    @Override
    public String toString() {
        return "Trompete";
    }
}