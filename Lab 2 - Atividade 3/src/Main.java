public class Main {
    public static void main(String[] args) {
        Instrumento violao = new Violao();
        Instrumento tambor = new Tambor();
        Instrumento trompete = new Trompete("Dó");

        Orquestra orquestra = new Orquestra();
        orquestra.add(violao);
        orquestra.add(tambor);
        orquestra.add(trompete);

        orquestra.play();

        orquestra.play(1);
    }
}