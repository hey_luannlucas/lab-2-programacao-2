# Orquestra - Visão Geral do Projeto

Este é um projeto de exemplo que implementa o conceito de uma orquestra utilizando programação orientada a objetos em Java. O objetivo é demonstrar o uso de herança, polimorfismo e abstração na modelagem de diferentes instrumentos musicais e sua interação em uma orquestra.

## Fluxograma:
![](Assests/Fluxograma%20orquestra.png)

## Diagrama de classes:
![](Assests/Diagrama%20de%20classes.png)


## Funcionalidades:

O projeto inclui as seguintes funcionalidades:

1. Definição de instrumentos musicais: Os instrumentos são modelados como classes que herdam de uma classe abstrata `Instrumento`. Cada instrumento implementa o método `play()` para reproduzir o som característico.

2. Adição de instrumentos à orquestra: A classe `Orquestra` permite adicionar instrumentos à sua coleção por meio do método `add()`, que recebe um objeto do tipo `Instrumento` como parâmetro.

3. Reprodução dos instrumentos: A classe `Orquestra` possui o método `play()`, que reproduz todos os instrumentos adicionados à orquestra. Além disso, é possível utilizar o método `play(int index)` para reproduzir apenas um instrumento específico com base no índice fornecido.

## Estrutura:

- O arquivo `Main.java` contém o ponto de entrada do programa, onde a orquestra é criada, os instrumentos são adicionados e os métodos `play()` são chamados para reproduzir os sons.

- A classe `Instrumento` é uma classe abstrata que define o contrato básico para os instrumentos musicais. Ela possui o método abstrato `play()`, que é implementado por cada instrumento concreto.

- As classes `Violao`, `Tambor` e `Trompete` são exemplos de instrumentos concretos que estendem a classe `Instrumento` e implementam o método `play()` de acordo com suas características específicas.

- A classe `Orquestra` é responsável por gerenciar os instrumentos adicionados a ela. Ela possui um campo `instrumentos` que é uma lista de objetos do tipo `Instrumento`. Através dos métodos `add()` e `play()`, é possível adicionar instrumentos à orquestra e reproduzir os sons de todos os instrumentos.


## Conclusão

Aqui ilustramos a aplicação de conceitos de polimorfismo, herança e abstração em um cenário de orquestra. Ele demonstra como criar e gerenciar diferentes instrumentos musicais, permitindo a reprodução dos sons individualmente ou em conjunto.
